<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PasienController extends Controller
{
    public function index()
    {

        return view('pasien');

    }

    public function tampil()
    {

    return view ('datapasien');

    }

    public function tambah()
    {

    return view('pasien');

    }

    public function store(Request $request)
    {
 
        DB::table('pasien')->insert([
        'id' => $request->id,
        'nama_pasien' => $request->nama,
        'alamat' => $request->alamat,
        'no_telp' => $request->telepon,
        'id_rs' => $request->idrs
        ]);

        return redirect('/Pasien');
    }
    

    public function edit($id)
    {
    
    $pasien = DB::table('pasien')->where('id', $id) ->get();
    return view('editpasien',['Pasien' => $pasien]);
    
    return redirect('/Pasien');
    }
    
    
    public function update(Request $request)
    {
    
    DB::table('pasien')->where('id',$request->id)->update([
    'id'=> $request->id,
    'nama_pasien'=> $request->nama,
    'alamat' => $request->alamat,
    'no_telp' => $request->telepon,
    'id_rs' => $request->idrs
    ]);
    
    return redirect('/Pasien');
    }

    public function hapus($id)
    {
    
    DB::table('pasien')->where('id',$id)->delete();
    return redirect('/Pasien');
    
    }




}
