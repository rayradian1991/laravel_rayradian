<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RumahSakitController extends Controller
{
    
public function index()
{

return view('rs');

}

public function tampil()
{
    return view('datars');
}


    public function tambah()
    {

    return view('rs');

    }

    public function store(Request $request)
    {
 
        DB::table('rs')->insert([
        'id' => $request->id,
        'nama_rs' => $request->nama,
        'alamat' => $request->alamat,
        'email' => $request->email,
        'telepon' => $request->telepon
        ]);

        return redirect('/RumahSakit');
    }
    
    public function edit($id)
    {
    
    $RumahSakit = DB::table('rs')->where('id', $id) ->get();
    return view('editrs',['RumahSakit' => $RumahSakit]);
    
    return redirect('/RumahSakit');
    }
    
    
    public function update(Request $request)
    {
    
    DB::table('rs')->where('id',$request->id)->update([
    'id'=> $request->id,
    'nama'=> $request->nama_rs,
    'alamat' => $request->alamat,
    'email' => $request->email,
    'telepon' => $request->telepon
    ]);
    
    return redirect('/RumahSakit');
    }
    
    public function hapus($id)
    {   

    DB::table('rs')->where('id',$id)->delete();
    return redirect('/RumahSakit');

    }

public function editRS()
{
    return view ('editrs');
}



    
}
