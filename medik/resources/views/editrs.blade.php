<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Rumah Sakit</title>
</head>
<body>
    
<h3> Edit Rumah Sakit </h3>
<a href="/RumahSakit">Kembali</a>

<br/>
<br/>

@foreach($RumahSakit as $p)
<form action="/RumahSakit/update" method="post">
    {{csrf_field()}}
 
    ID    : <input type="text" required="required" name="id" value="{{$p->id}}"><br/>
    Rumah Sakit    : <input type="text" required="required" name="nama" value="{{$p->nama_rs}}"><br/>
    Alamat  : <textarea required="required" name="alamat">{{$p->alamat}}</textarea> <br/>
    Email : <input type="text" required="required" name="email" value="{{$p->email}}"><br/>
    Telepon  : <input type="number" required="required" name="telepon" value="{{$p->telepon}}"><br/>
    
    <input type="submit" value="Simpan Data">
</form>
@endforeach





</body>
</html>