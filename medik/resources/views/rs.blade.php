<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Rumah Sakit</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>

      .mx-auto {width:800px}
      .card { margin-top:10px;}

   </style>

  </head>
<body>

<div class="mx-auto"> 

<h3> Data Rumah Sakit </h3>

<a href="/RumahSakit/tampil">Lihat Data Rumah Sakit </a>
<div class="card">
<div class="card-header">
    Tambah Data
</div>
  <div class="card-body">
<form action="RumahSakit/store" method="post">
    {{csrf_field()}} 
  <div class="form-group">
    <label for="id">ID : </label> <input type="text" class="form-control" id="id" name="id">  
  </div>

  <div class="form-group">
    <label for="nama">Nama Rumah Sakit : </label> <input type="text" class="form-control" id="nama" name="nama">  
  </div>
  
  <div class="form-group">
    <label for="alamat">Alamat  :</label> <textarea class="form-control" id="alamat" rows="3" name="alamat"></textarea>
  </div>

  <div class="form-group">
    <label for="email">Email : </label> <input type="text" class="form-control" id="email" name="email">  
  </div>
  
  <div class="form-group">
    <label for="telepon">Telepon : </label> <input type="text" class="form-control" id="telepon" name="telepon">  
  </div>
  <div class="col-sm-10">

    <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">

</div>
</form>
</div>
</div>


</div>
    
</body>
</html>

