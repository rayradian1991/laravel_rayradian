<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Pasien</title>
</head>
<body>
    
<h3> Edit Pasien </h3>
<a href="/Pasien">Kembali</a>

<br/>
<br/>

@foreach ($pasien as $p)
<form action="/Pasien/update" method="post">
    {{csrf_field()}}
    
    ID    : <input type="text" required="required" name="id" value="{{$p->id}}"><br/>
    Nama Pasien    : <input type="text" required="required" name="nama" value="{{$p->nama_pasien}}"><br/>
    Alamat  : <textarea required="required" name="alamat">{{$p->alamat}}</textarea> <br/>
    Telepon : <input type="number" required="required" name="telepon" value="{{$p->no_telp}}"><br/>
    ID RS  : <input type="number" required="required" name="idrs" value="{{$p->id_rs}}"><br/>
    
    <input type="submit" value="Simpan Data">
</form>
@endforeach





</body>
</html>