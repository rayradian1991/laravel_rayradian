<?php
$host ="localhost";
$user ="root";
$pass ="";
$db = "medis";

$koneksi =mysqli_connect($host,$user,$pass,$db);
if (!$koneksi){
    die("tidak bisa terkoneksi ke database");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Pasien</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>

<div class="card">
  <div class="card-header text-white bg-secondary">
    Data Pasien
  </div>

  <div class="card-body">
  <table class="table">

  <thead>
   <tr>

    <th scope="col">ID</th>
    <th scope="col">Nama Pasien</th>
    <th scope="col">Alamat</th>
    <th scope="col">No.Telp</th>
    <th scope="col">ID RS</th>
    <th scope="col" class="col-2">Aksi</th>

  </tr>

<tbody>

<?php 


$sql = "select * from pasien";
$q2     = mysqli_query($koneksi,$sql);

while($r2= mysqli_fetch_array($q2)){

$id       = $r2['id'];
$nama      = $r2['nama_pasien'];
$alamat     = $r2['alamat'];
$telepon   = $r2['no_telp'];
$idrs = $r2['id_rs'];

?>

<tr>
<th scope="row"><?php echo $id?></th>
<td scope="row"><?php echo $nama?></td>
<td scope="row"><?php echo $alamat?></td>
<td scope="row"><?php echo $telepon?></td>
<td scope="row"><?php echo $idrs?></td>
<td scope="row">  
<a href="/Pasien/edit"><button type="button" class="btn btn-danger btn-sm">Edit</button></a>
<a href="/Pasien/hapus" onclick ="return confirm('Yakin mau delete data?')"><button type="button" class="btn btn-warning btn-sm">Delete</button></a>
</td>
</tr>


<?php
}
?>


</tbody>

  </thead>

  </table>

</body>
</html>