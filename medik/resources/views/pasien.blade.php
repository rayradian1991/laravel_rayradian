<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Pasien</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>

    .mx-auto {width:800px}
    .card { margin-top:10px;}

    </style>


  </head>
<body>
<div class="mx-auto">

<h3> Data Pasien </h3>
<a href="/Pasien/tampil">Lihat Data Semua Pasien </a>
<div class="card">
<div class="card-header">
    Tambah Data
</div>
  <div class="card-body">
<form action="/Pasien/store" method="post">
    {{csrf_field()}} 
  <div class="form-group">
    <label for="id">ID : </label> <input type="text" class="form-control" id="id" name="id">  
  </div>

  <div class="form-group">
    <label for="nama">Nama Pasien : </label> <input type="text" class="form-control" id="nama" name="nama">  
  </div>
  
  <div class="form-group">
    <label for="alamat">Alamat  :</label> <textarea class="form-control" id="alamat" rows="3" name="alamat"></textarea>
  </div>

  <div class="form-group">
    <label for="telepon">No.Telepon : </label> <input type="text" class="form-control" id="telepon" name="telepon">  
  </div>
  
  <div class="form-group">
    <label for="idrs">ID Rumah Sakit : </label> <input type="text" class="form-control" id="idrs" name="idrs">  
  </div>
  <div class="col-sm-10">

    <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">

</div>
</form>
</div>
</div>


</div>
    
</body>
</html>

