<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/RumahSakit', 'RumahSakitController@index');

Route::get('/RumahSakit/tambah','RumahSakitController@tambah');
Route::post('/RumahSakit/store','RumahSakitController@store');
Route::get('/RumahSakit/tampil','RumahSakitController@tampil');
Route::get('/RumahSakit/edit/{id}','RumahSakitController@edit');
Route::post('/RumahSakit/update','RumahSakitController@update');
Route::get('/RumahSakit/delete/{id}','RumahSakitController@hapus');
Route::get('/RumahSakit/editRS','RumahSakitController@editRS');

Route::get('/Pasien', 'PasienController@index');

Route::get('/Pasien/tambah','PasienController@tambah');
Route::post('/Pasien/store','PasienController@store');
Route::get('/Pasien/tampil','PasienController@tampil');
Route::get('/Pasien/edit/{id}','PasienController@edit');
Route::post('/Pasien/update','PasienController@update');
Route::get('/Pasien/delete','PasienController@hapus');

Route::get('/Login','LoginController@index');



